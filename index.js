const x = 10;
const y = 100;

// comparison lebih dari
const isBiggerThanY = x > 10;
console.log(isBiggerThanY);

//------------------------------------

const person = {
	name : 'Seu',
	gender : 'Laki-laki',
	age : 20
}
const isFemale = person.gender == 'Perempuan';
console.log(isFemale);

const isYoung = person.age <= 20;
console.log(isYoung);

//-------------------------------------

if (isYoung) {
	console.log(`${person.name} itu muda`);
}
else{
	console.log(`${person.name} itu tua`);
}
