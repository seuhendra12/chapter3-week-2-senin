const stockFruits = ["Manggo", "Orange", "Banana", "Coconut"];
const person = {
	name : 'Seuhendra Setiawan',
	ktpNumber : '12341231',
	age : 20
}

const isMyFavoriteFruitInStocks = "Watermelon" in stockFruits;
console.log(isMyFavoriteFruitInStocks);

const doesPersonHaveKTP = "ktpNumber" in person;
console.log(person.name, doesPersonHaveKTP ? "Has KTP" : "Does not have KTP");

console.log(person instanceof Date);

const date = new Date();
console.log(date instanceof Date);